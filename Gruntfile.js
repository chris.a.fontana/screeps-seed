module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-screeps');

    grunt.initConfig({
        screeps: {
            options: {
                email: 'email',
                password: 'password',
                branch: 'grunt',
                ptr: false
            },
            dist: {
                src: ['dist/*.js']
            }
        }
    });

};